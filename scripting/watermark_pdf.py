import PyPDF2
import sys


def watermark_pdf(path_pdf, path_watermark_pdf):
    with open(path_watermark_pdf, 'rb') as fh_watermark:
        reader_watermark = PyPDF2.PdfFileReader(fh_watermark)
        watermark = reader_watermark.getPage(0)

        with open(path_pdf, 'rb') as fh_pdf:
            reader = PyPDF2.PdfFileReader(fh_pdf)
            writer = PyPDF2.PdfFileWriter()

            num_pages = reader.getNumPages()

            for page_number in range(num_pages):
                page = reader.getPage(page_number)
                page.mergePage(watermark)  # in-place

                writer.addPage(page)

            with open('./pdfs/watermarked.pdf', 'wb') as fh_output:
                writer.write(fh_output)


if __name__ == '__main__':
    try:
        path_pdf, path_watermark_pdf = sys.argv[1:]
    except:
        sys.exit('Invalid command. Use: `watermark_pdf.py <pdf> <watermark>')

    watermark_pdf(path_pdf, path_watermark_pdf)
