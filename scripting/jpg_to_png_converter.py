import pathlib
import sys
import os
from PIL import Image

if __name__ == '__main__':
    # graph first and second argument

    try:
        input_folder, output_folder = sys.argv[1:]
    except:
        sys.exit('Invalid command. Use: `jpg_to_png_converter <input_folder> <output_folder>')
        

    # check if output foldre exists. If not create it.
    if not os.path.exists(output_folder):
        os.mkdir(output_folder)

    # loop through input folder
    for filename in os.listdir(input_folder):
        basename = pathlib.PurePath(filename).stem
        
        with Image.open(os.path.join(input_folder, filename)) as img:
            
            # save the converted images in output folder
            # convert imgaes to png
            img.save(os.path.join(output_folder, basename + '.png'), 'png')

