import requests
import hashlib
import sys
from requests.exceptions import MissingSchema


def get_hashed_password(password):
    return hashlib.sha1(password.encode('utf-8')).hexdigest().upper()


def request_api_data(query_chars):
    url = 'https://api.pwnedpasswords.com/range/' + query_chars
    try:
        res = requests.get(url)
        if res.status_code != 200:
            raise RuntimeError(
                f'Error fetching: {res.status_code}, check the api and try again!')
        return res
    except MissingSchema:
        print('incorrect url: ', url)


def read_response(response):
    return response.text


def get_password_leaks_count(hashes, hash_to_check):
    hashes = (line.split(':') for line in hashes.text.splitlines())
    for h, count in hashes:
        if h == hash_to_check:
            return count
    return 0


def pwned_api_check(password):
    hashed_password = get_hashed_password(password)
    first5_char, tail = hashed_password[:5], hashed_password[5:]
    # Note: api takes 5 first chars of hashed password! Otherwise it returns a 400
    response = request_api_data(first5_char)
    hashes = read_response(response)
    #  check if password exists in API response.
    return get_password_leaks_count(response, tail)


def main(args):
    for password in args:
        count = pwned_api_check(password)
        if count:
            print(
                f'{password} was found { count} times. You should change your password!')
        else:
            print(f'{password} was NOT found. All good.')


if __name__ == '__main__':
    args = sys.argv[1:]
    if len(args) < 1:
        sys.exit('No passwords were given. Exit.')

    main(args)
