from PIL import Image, ImageFilter


with Image.open("./pokedex/pikachu.jpg") as img:
    print(img.format)
    print(img.size)
    print(img.mode)

    filtered_img = img.copy()
    filtered_img = filtered_img.filter(ImageFilter.BLUR)
    filtered_img.save('./outputs/blur.png', 'png')

    filtered_img = img.copy()
    filtered_img = filtered_img.filter(ImageFilter.SMOOTH)
    filtered_img.save('./outputs/smooth.png', 'png')

    filtered_img = img.copy()
    filtered_img = filtered_img.filter(ImageFilter.SHARPEN)
    filtered_img.save('./outputs/sharpen.png', 'png')

    filtered_img = img.copy()
    filtered_img = filtered_img.convert('L')
    filtered_img.save('./outputs/grey.png', 'png')

    filtered_img = img.copy()
    filtered_img = filtered_img.rotate(90)
    filtered_img.save('./outputs/rotate90.png', 'png')

    filtered_img = img.copy()
    filtered_img = filtered_img.resize((300, 300))
    filtered_img.save('./outputs/resized.png', 'png')

    filtered_img = img.copy()
    filtered_img = filtered_img.crop((100, 100, 400, 400))
    filtered_img.save('./outputs/crop.png', 'png')
    filtered_img.show()

with Image.open("./images/astro.jpg") as img:
    img.thumbnail((400, 400)) # Keeps aspect ratio
    img.save('./outputs/astro_thumbnail.jpg') 
