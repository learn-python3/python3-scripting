import os
import time
import tweepy
from dotenv import load_dotenv
load_dotenv(verbose=True)

CONSUMER_KEY = os.getenv('CONSUMER_KEY')
CONSUMER_SECRET = os.getenv('CONSUMER_SECRET')
ACCESS_TOKEN = os.getenv('ACCESS_TOKEN')
ACCESS_TOKEN_SECRET = os.getenv('ACCESS_TOKEN_SECRET')


auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
auth.set_access_token(ACCESS_TOKEN, ACCESS_TOKEN_SECRET)

api = tweepy.API(auth)

# public_tweets = api.home_timeline()
# for tweet in public_tweets:
#     print(tweet.text)

user = api.me()
print('---- USER ---')
print(user.name)
print(user.screen_name)
print(user.followers_count)
print('---- END OF USER ---\n')


def limit_handler(cursor):
    while True:
        try:
            yield cursor.next()
        except tweepy.RateLimitError:
            time.sleep(1000)
        except StopIteration:
            break


# Generous Bot - always follow back
print('---- FOLLOWERS ---')
for follower in limit_handler(tweepy.Cursor(api.followers).items()):
    follower.follow()
    print(follower.name)

print('---- END OF FOLLOWERS ---\n')
