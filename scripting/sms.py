import os
from twilio.rest import Client
from dotenv import load_dotenv
load_dotenv(verbose=True)

TWILIO_ACCOUNT_SID = os.getenv('TWILIO_ACCOUNT_SID')
TWILIO_AUTH_TOKEN = os.getenv('TWILIO_AUTH_TOKEN')
TWILIO_PHONE_NUMBER = os.getenv('TWILIO_PHONE_NUMBER')
PRIVATE_PHONE_NUMBER = os.getenv('PRIVATE_PHONE_NUMBER')

account_sid = TWILIO_ACCOUNT_SID
auth_token = TWILIO_AUTH_TOKEN
client = Client(account_sid, auth_token)

message = client.messages.create(
    from_=TWILIO_PHONE_NUMBER,
    to=PRIVATE_PHONE_NUMBER,
    body="A Message from my twilio service."
)

print(message.sid)
