
import sys
import smtplib
from email.message import EmailMessage
from string import Template
from pathlib import Path

'''
NOTE: For this to work you have to lower the gmail security settings.
In your gmail security settings, activate the `access by less secure apps`
switch.
https://myaccount.google.com/u/1/security
'''


def send_email(user, password):
    html = Template(Path('email_template.html').read_text())
    email = EmailMessage()
    email['from'] = 'Sarah Connor'
    email['to'] = 'mirkolauff@gmail.com'
    email['subject'] = 'You are a winner!'

    # email.set_content('I was sent with Python. Cool, right?')

    # instead of sending text, we now send an 'html'.
    # Second parameter sets type of text to html.
    email.set_content(html.substitute(name='Mirko Lauff'), 'html')

    with smtplib.SMTP(host='smtp.gmail.com', port=587) as smtp:
        smtp.ehlo()
        smtp.starttls()
        smtp.login(user, password)
        smtp.send_message(email)
        print('email sent.')


if __name__ == '__main__':
    try:
        user, password = sys.argv[1:]
    except:
        sys.exit('Error! Use script `send_email <user> <password>')

    send_email(user, password)

# Lx3P1x7bP09U
